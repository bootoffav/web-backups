"""
Creates gzipped archives of root www directory and specified mysql databases.
Stores it in DO Spaces.

Usage:

    python3 backup.py
"""

import os
from subprocess import run, Popen, PIPE, DEVNULL
from datetime import date, timedelta
from time import sleep

from config import CFG


def existing_backups():  # covered
    '''list files on backup storage

    Returns:
        A generator instance of dictionaries containing
        name, date keys for each backup(file)
    '''
    s3cmd_ls = (line.decode('utf-8').strip()
                for line in Popen(['s3cmd', 'ls', '-r', CFG['backup_rootdir']],
                                  stdout=PIPE).stdout)

    return ({'date': line.split()[0], 'name': line.split()[-1]}
            for line in s3cmd_ls if line.split()[2] != '0')


def delete_old_backups():
    '''Produces 2 checks:
    1. File creation date is < than current date - 15 days
    2. If file creation date is at first day of month
        then keeps this file for last 6 months
    '''
    old_keep_daily = date.today() - timedelta(CFG['keep_daily'])
    old_keep_montly = date.today() - timedelta(CFG['keep_monthly'])

    for cur_file in existing_backups():
        file_date = date.fromisoformat(cur_file['date'])

        if file_date.day == 1 and file_date < old_keep_montly:
            Popen(('s3cmd', 'del', cur_file['name']))
        elif file_date < old_keep_daily:
            Popen(('s3cmd', 'del', cur_file['name']))


def backup_db():
    '''Backup databases'''
    for db_name in SqlBackup.dbnames():
        SqlBackup(root_dir=CFG['backup_rootdir'] +
                  '/mysql', ext='sql', target=db_name)


def backup_dir():
    '''Backup directories recursively, max-depth=1'''
    for main_dir, sub_dirs in DirBackup.folders():
        for folder in sub_dirs:
            DirBackup(root_dir=CFG['backup_rootdir'] + '/' + main_dir,
                      ext='tar', target=folder, main_dir=main_dir)


class Backup():
    '''Main Backup class'''

    def __init__(self, **params):
        self.params = params
        self.zipped = self.zip()
        self.encoded = self.encode()
        uploading = self.upload()
        uploading.wait()

    def zip(self):
        '''gzip upcoming stream'''
        return Popen(('gzip', '-9'),
                     stdin=self.stream.stdout, stdout=PIPE,
                     stderr=DEVNULL)

    def encode(self):
        '''gpg assymetric encoding'''
        return Popen(('gpg', '-c', '--batch', '--passphrase', CFG['passphrase']),
                     stdin=self.zipped.stdout, stdout=PIPE,
                     stderr=DEVNULL)

    def upload(self):
        '''upload to s3 storage'''
        return Popen(('s3cmd', 'put', '-',
                      '{0}/{1}/{1}-{2}.{3}.gz.gpg'.format(self.params['root_dir'],
                                                          self.params['target'],
                                                          CFG['date'], self.params['ext'])
                      ), stdin=self.encoded.stdout, stdout=DEVNULL)


class DirBackup(Backup):
    '''directory derived backup class.'''
    @property
    def stream(self):
        '''stream data from source'''
        return Popen(('tar', '-cP',
                      '{0}/{1}/{2}'.format(CFG['dir_path'], self.params['main_dir'], self.params['target'])),
                     #  "{0}/{p['main_dir']}/{p['target']}".format(CFG['dir_path'], p=self.params)),
                     stdout=PIPE, stderr=DEVNULL)

    @staticmethod
    def folders():
        '''Get all subdirectories'''
        for folder in CFG['dirs']:
            yield (folder, tuple(name for name in os.listdir(CFG['dir_path'] + os.sep + folder)
                                 if os.path.isdir(os.path.join(CFG['dir_path'] + os.sep + folder,
                                                               name))))


class SqlBackup(Backup):
    '''mysql specific backup class.'''
    @property
    def stream(self):
        '''stream data from source'''
        return Popen(('mysqldump', '-u%s' % CFG['mysql_user'], '-p%s' % CFG['mysql_passwd'],
                      self.params['target']),
                     stdout=PIPE, stderr=DEVNULL)

    @staticmethod
    def dbnames():
        ''' Ask mysql-server for all databases.'''
        exclude_dbs = ('information_schema', 'mysql', 'performance_schema')

        dbs = run(('mysql', '-u{}'.format(CFG['mysql_user']),
                   '-p{}'.format(CFG['mysql_passwd']),
                   '-s', '-e', 'show databases;'),
                  stdout=PIPE, stderr=DEVNULL, encoding='UTF-8').stdout.split()
        return [db for db in dbs if db not in exclude_dbs]


if __name__ == '__main__':
    delete_old_backups()
    backup_db()
    backup_dir()
