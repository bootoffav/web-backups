'''Configuration for backups'''
import datetime

DATE = datetime.datetime.now()

CFG = {
    'mysql_user': 'root',
    'date': str(DATE.date()) + '__' + str(DATE.hour) + str(DATE.minute),
    'passphrase': 'jcli50LqLTjqx3dyjJYoLiHXTxLn/kPK',
    'keep_daily': 16,
    'keep_monthly': 185  # days
}

PROD_CFG = {
    'backup_rootdir': 's3://web-backups',
    'mysql_passwd': 'SxXm*N?*tY*vGwJ^RLv2XtXc',
    'dir_path': '/var/www',
    'dirs': ('xmgarment', 'xmtextile', 'xmsilverline', 'xmfireline')
}

DEV_CFG = {
    'backup_rootdir': 's3://web-backups/dev',
    'mysql_passwd': 'root',
    'dir_path': '/Users/bootoffav/dev/python',
    'dirs': ('checkIO', 'httpie'),
}

CFG.update(PROD_CFG)
