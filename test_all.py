'''Test for backup module'''
import unittest
import subprocess
import backup

from config import CFG

class ModuleFunctionsTest(unittest.TestCase):
    def test_existing_backups(self):
        '''checks if every element of generator contains name, date.'''
        for file in backup.existing_backups():
            self.assertTrue(set(['name', 'date']).issubset(file.keys()))

    def test_dbnames_does_not_contain_service_databases(self):
        dbnames = backup.SqlBackup.dbnames()
        excluded_db = {'information_schema', 'mysql', 'performance_schema'}
        self.assertTrue(excluded_db.isdisjoint(dbnames))


class SqlBackupTest(unittest.TestCase):
    def test_backup_db(self, target='larademos'):
        filename = CFG['backup_rootdir'] + '/mysql/{0}/{0}-'.format(target) + \
                   CFG['date'] + '.sql.gz.gpg'
        backup.SqlBackup(root_dir=CFG['backup_rootdir'] + '/mysql', ext='sql', target=target)
        self.assertIn(filename, [f['name'] for f in backup.existing_backups()])
        subprocess.run(('s3cmd', 'del', filename))

class DirBackup(unittest.TestCase):
    pass

if __name__ == '__main__':
    unittest.main()
